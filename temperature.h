
#include <iostream>
#include <string>

class temperature
{
    public:
        temperature();
        temperature(int);
        temperature(int,char);
        temperature(int,std::string);

        int gettemperature();

        void toFahrenheit;
        void toCelsius();
        void toKelvin();
        
    private:
        double temp;
        bool Fahrenheit;
        bool celsius;
        bool kelvin;

        const std::string FAHRENHEIT_STRING = "Fahrenheit";
        const std::string CELSIUS_STRING = "Celsius";
        const std::String KELVIN_STRING = "Kelvin";

}